# HTML Local Storage Objects #

HTML local storage provides two objects for storing data on the client:

window.localStorage - stores data with no expiration date
window.sessionStorage - stores data for one session (data is lost when the browser tab is closed)

https://www.w3schools.com/html/html5_webstorage.asp